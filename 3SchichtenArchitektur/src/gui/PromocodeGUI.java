package gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import javax.swing.SwingConstants;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.IOException;

import logic.IPromoCodeLogic;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JLabel lblPromoCode;
	private JPanel contentPane;
	private Font ea;

	public PromocodeGUI(IPromoCodeLogic logic) {
		
		try {
			ea = Font.createFont(Font.TRUETYPE_FONT, new File("EA Logo.ttf")).deriveFont(35f);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("EA Logo.ttf")));
		}
		catch(IOException | FontFormatException e) {
			
		}
		
		setTitle("$$$ Promotioncode-Generator $$$");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 200);
		
		setIconImage(new ImageIcon(getClass().getResource("ea-logo.jpg")).getImage());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBackground(Color.BLACK);
		
		lblPromoCode = new JLabel("");
		lblPromoCode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromoCode.setFont(new Font("Arial", Font.BOLD, 20));
		lblPromoCode.setForeground(Color.WHITE);
		lblPromoCode.setText("XXXXX-XXXXX-XXXXX-XXXXX-XXXXX");
		
		contentPane.add(lblPromoCode, BorderLayout.CENTER);
		
		
		JButton PromoCodeNeu = new JButton("$$$ Neuen Promotioncode (Profit) generieren $$$");
		PromoCodeNeu.setBackground(Color.WHITE);
		PromoCodeNeu.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		
		PromoCodeNeu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblPromoCode.setText(code);
			}
		});
		
		contentPane.add(PromoCodeNeu, BorderLayout.SOUTH);
		
		JLabel lblLogoE = new JLabel("EA");
		lblLogoE.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogoE.setFont(ea);
		lblLogoE.setForeground(Color.WHITE);
		contentPane.add(lblLogoE, BorderLayout.EAST);
		
		JLabel lblLogoW = new JLabel("EA");
		lblLogoW.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogoW.setFont(ea);
		lblLogoW.setForeground(Color.WHITE);
		contentPane.add(lblLogoW, BorderLayout.WEST);
		
		JLabel lblShowPromoCode = new JLabel("Promotioncode f�r einen EXP-Boost im Unterrichts-Block");
		lblShowPromoCode.setHorizontalAlignment(SwingConstants.CENTER);
		lblShowPromoCode.setFont(new Font("Arial", Font.PLAIN, 20));
		lblShowPromoCode.setForeground(Color.WHITE);
		contentPane.add(lblShowPromoCode, BorderLayout.NORTH);
		this.setVisible(true);
		this.setResizable(false);
	}

}
