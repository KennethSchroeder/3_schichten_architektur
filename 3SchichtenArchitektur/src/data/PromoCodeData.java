package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {

	private List<String> promocodes;
	private String fileName = "PromoCodesListe.txt";

	public PromoCodeData() {
		this.promocodes = new LinkedList<String>();
		this.getPromoCodeFromFile();
	}

	private boolean getPromoCodeFromFile() {
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
			String line = bufferedReader.readLine();
			while (line != null) {
				promocodes.add(line);
				line = bufferedReader.readLine();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

	@Override
	public boolean savePromoCode(String code) {
		return saveToFile(code) && this.promocodes.add(code);
	}

	private boolean saveToFile(String code) {
		try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true))) {
			bufferedWriter.write(code);
			bufferedWriter.newLine();
			return true;
		} catch (IOException e) {
			return false;
		}
	}
}
